import { Routes } from '@angular/router';
import { AddCustomerComponent } from './components/add-customer/add-customer.component';
import { EditCustomerComponent } from './components/edit-customer/edit-customer.component';
import { ListCustomerComponent } from './components/list-customer/list-customer.component';

export const ROUTES: Routes = [
    { path: 'add-empleado', component: AddCustomerComponent },
    { path: 'edit-empleado', component: EditCustomerComponent },
    { path: 'list-empleado', component: ListCustomerComponent },
    { path: '', pathMatch: 'full', redirectTo: 'list-empleado' },
    { path: '**', pathMatch: 'full', redirectTo: 'list-empleado' }
];
